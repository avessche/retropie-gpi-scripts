#! /bin/bash

rsync -a /boot/roms/* /home/pi/RetroPie/roms/
chown -R pi:pi /home/pi/RetroPie/roms
rm -rf /boot/roms/*

exit 0
