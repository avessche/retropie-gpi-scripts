# retropie bootscripts

## Description
Set of simple scripts that you can use to facilitate magaging retropi on raspberry pi's that have no internet access like the Raspberry pi zero in the Retrogflag GPI case.
We will be using the `/boot` directory as this will be accessible in windows when you instert the SD card into a windows PC.

## Install
1. Follow the regular install by downloading the retropi image for the pi0.
1. Write the image to the SD card as you used to do.
1. follow the procedure to enable you pi as ethernet gadget https://learn.adafruit.com/turning-your-raspberry-pi-zero-into-a-usb-gadget/ethernet-gadget
1. Don't forget to enable ssh by adding an empty file called 'SSH' into the boot.
1. Boot the pi and login using ssh to pi@retropie.local
1. Make sure you bridge your internet connection in windows between you LAN or WiFi and the ethernet gadget interface
1. Continue to follow the pocedure to add the gpi modifications for the safe-shutdown and display script. Do not build it into the case yet. http://download.retroflag.com/manual/case/GPi_CASE_Manual.pdf
1. Copy over the scripts you find here
```
/boot/bootscripts/
 - copyroms.sh
/etc/init.d/
 - bootscripts.sh
 mkdir /boot/roms
```

9. add the execution flag to the files 
``` 
chmod a+x /boot/bootscripts/*
chmod a+x /etc/init.d/bootscript.sh
```
10. shutdown the pi zero and continue the build of the case.
1. boot up the case and follow the first instructions to map the buttons

## Usage
Copy the roms in the `/boot/roms/<emulatordir>/`. At boot the roms fill automatically be copied over to the right dir at boot and should be visible after boot.

_Example:_
```
/boot/roms/gba/coolgame.gba
/boot/roms/gb/anothergame.gb
```

You can add you own optional scripts that needs to run at boot in the `/boot/bootscripts/` folder.
