#! /bin/bash

### BEGIN INIT INFO
# Provides:          bootscript
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs 
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Run bootscript
# Description:       Run scripts in /boot/bootscript/ during boot
### END INIT INFO

# Carry out specific functions when asked to by the system
case "$1" in
  start)
    source /boot/bootscript/*.sh
    ;;
  stop)
    ;;
  *)
    echo "Usage: /etc/init.d/bootscript {start|stop}"
    exit 1
    ;;
esac

exit 0
