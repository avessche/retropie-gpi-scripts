<!-- The first sections: "Problem to solve" and "Proposal", are strongly recommended. However, keep in mind that providing complete and relevant information early helps validate the problem and start working on a solution. -->

### Problem to solve

<!-- What problem do we solve? -->

### Proposal

<!-- How are we going to solve the problem? -->

### Further info

/label ~"Feature Request"
/assign @avessche
